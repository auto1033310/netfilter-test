
nqueue=-lnetfilter_queue

all: netfilter-test

netfilter-test: netfilter-test.o
	$(LINK.cc) $^ $(nqueue) -o $@

clean:
	rm -f *.o netfilter-test